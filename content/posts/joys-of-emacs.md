---
author: "Akbar M"
title: "The joys of emacs"
date: "2020-07-15"
description: "Anything text - Grok on emacs. Never has this failed in decades of "
draft: true
tags: [
    "emacs",
    "org-mode",
    "algae",
    "tank",
    "maintainance",
    ]
---

# The Joys of Emacs


I live on emacs, write code, organize my days, manage tasks, and even journal on emacs. The years spent using have only reinforced my affinity towards emacs. The distraction-free, close-to-the-metal operations are refreshing to experience. 

I'm a statistician and in the past relied on (R)[https://www.r-project.org/] for all statistical computing requirements. Well, apart from SAS and python. Not many editors or IDEs were available except [Emacs with ESS](https://ess.r-project.org/). Here is where the magic started. Many will say the learning curve for emacs is steep, and I agree. Emacs is not something I'd first recommend to a newly minted data scientist or its ilk. Unless being nimble, swift, and focused are part of your programmer conditioning.


The sins of holding a [golden hammer](https://en.wikipedia.org/wiki/Law_of_the_instrument). 
I remember an online curriculum that required me to install eclipse or IntelliJ idea to work with scala. I dabbled in eclipse for a few days installing all the pre-requisites, and when I finally began to code it seemed quite odd where at the end of a code block or completiong of module there is a flourish of clicks and 

I have since lived on other shiny tools. Sometimes limited by project environments, security policies, or team dynamics that limit reliance on arcane tools or social constructs that give preference to some over others. Despite these, it is hard to beat emacs with its simplicity and power. I have heard great things about the VI, I, however, avoided crossing over to the dark side. The thought itself of learning a new tool and revising a productive setup felt quite draining. The temptation remains. Many environments come bundled with vim than emacs.

I envy the dark wizards who brought their arts to the light side. I write this article using emacs running on top of a smart framework (Doom)[https://github.com/hlissner/doom-emacs], built by a former vimmer. Doom has allowed me to retire my old configs, and I find much of what I need for my daily productivity. The config framework minimizes the need to recreate environments and bundling packages for everyday programming. I still rely on the non-vim experience and have no complaints ever since moving here. 

As a social creature, I lean on common instruments and perhaps more prevalent in the organizations that I burrow. I find it hard to avoid context switching with other applications. I barely use org-mode to schedule tasks or use the emacs email services. It is far more convenient to rely on more modern applications for emails and scheduling and perhaps less impolite when with colleagues. 

Some habits are hard to let go. I still rely on org-mode for journaling and expanding my slip notes in emacs. I'm yet to see a more cleanly implemented app for note-taking and journaling. 

Remote shells are a nightmare. 
The only sour patch in my emacs fondness is working with remote shells in a private cloud. Anything from sshing into a terminal to opening files remotely freezes the editor. A neat config that can swiftly land me in a remote location without worry (about password encryption policies, bash login messages, operating system nuances) is yet to come by. Thankfully, this is less of an issue while working with remote shells on the cloud 


I occasionally run into a fellow emacser or a vim user relying on evil mode and rarely do i come out without learning something new. 





