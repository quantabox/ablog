---
author: "akbar m"
title: "Zettelkasten Apps"
date: "2020-07-02"
description: "Apps with zettelkasten workflows"
draft: true
tags: [
    "productivity",
    "note taking",
    "zettelkasten",
    "apps",
    "slip box"
]
---

