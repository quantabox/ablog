---
author: "akbar m"
title: "Ergonomic Accesories"
date: "2020-07-02"
description: "Venturing into the world of egonomic accessories"
draft: true
tags: [
    "ergonomic",
    "accessories",
    "trackball",
    "index finger"
]
---

Ever since I've started working remotely, my interactions with technology started to affect my health. I often see myself taking my laptop to the bed, alligning my posture to accomodate weird charging portt locations and trying not to bother my partner. Sometimes it would be on the sofa too. Now all of this for 8+ hours brings in some level of risk towards repetitive stress injury (RSI). What is RSI? [Here](https://www.nhs.uk/conditions/repetitive-strain-injury-rsi/) is a nice summary of the issue.  

### Common problems of a knowledge worker

I started to notice my wrist becoming stiff and in turn I would change my posture that would in turn stressed my back, shouder and neck - No beuno!. 

Now, I'm no where close to be in pain but discomfort was fairly apparent. this lead me to look for preventive measures and solutions - my first target happened to be my wrist. I really wanted a solution in which i didn't have to move my hand. the clawlike grip with a a bent wrist and 
