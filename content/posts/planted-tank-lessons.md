---
author: "AHM"
title: "What I learned keeping an outdoor planted tank!"
date: "2020-07-15"
description: "Notes on keeping a low-tech, outdoor planted tank"
tags: [
    "planted",
    "aquarium",
    "algae",
    "tank",
    "maintainance",
    ]
---


I've now managed to keep a thriving planted tank for more than two years. It is quite rewarding to have a  thriving ecosystem in a waterbox. I'm going to make an attempt to share some of my learnings as i have fumbled to maintain balance in this man made contraption. Should you find yourself in a similar journey, I hope you find this useful .

## One odd specification - An outdoor aquarium 
The planted tank needed to be outdoors. The limited indoors space mean't I could only salvage what space remained in the patio. I assure you, several measurments were taken to ensure a habitable ecosystem and it is safe for the water beings. I'll write another article to highlight parameters that you need to measure before you begin a planted tank.

## The equipment
You will generally need three ingredients fertilizers, CO2 and strong lighting for a planted tank (apart from the water box itself). much of the initial fertilizers will come from the ADA amazonia soil. 

Here are the items I used: 
* 200 Liters (~55 Gallon) Glass tank - Custom built
* ADA Amazonia soil
* Natural river gravel
* Top filter - this is a commonly available filter in Asia. Quite effective!

What i didn't intend to use:
* CO2 injection - With the available space and maintaince required. I avoided additional investment in CO2 equipment. you will need a good understanding of what makes a low tech tank work well to have lush, healthy environment without requiring CO2 injection. I'll cover more in the following sections.
* Lighting fixture - Natural light will be a primary light source. This meant the aquarium will be subject to natural weather patterns. Plant growth will be affected and balance in the tank water parameters may take longer than other more technology driven approaches.

Lessons: 
1. Shallow tanks are better, it allows better light penetration and is easier to manage during maintainance.
2. Understand [buffered substrate]() potency before making a purchase. I made a mistake of underestimating the potency of ADA amazonia soil. it has more nutrition that the plant mass I planned to utilize. I'd recommend using a buffered substrate only in moderation.
3. When choosing lighting fixtures a medium light level of 30-50 umol is good enough. T5 flouresent lamps will do. Higher light levels could invite algae blooms. 


## The Macrophytes - Aquatic Plants

A lack of CO2 and synthetic lighting equipment meant that I need to be more delibrate with plant selection. I invested a considerable amount of time assessing various aquatic plant species that could thrive in a natural environment. 

The key parameters for the selection included: 
* Availability - I needed to source the plants locally and this may seem obvious however there are nuances to this and quite specific for an outdoor aquarium that I will discuss in the following sections.
* Hardy plants - I wanted plants that can withstand the tropical weather and a bright Indian sun
* Fast growing - I naively added this requirement to combat algae! 

The plants I choose:
1. Anubias nana
2. Vallisneria nana
3. Sagittaria subulata
4. Microsorum Pteropus - Java Fern
5. Pistia stratiotes (Floating plant)
6. Salvinia natans (Floating plant)
7. 

## Balance in a Planted Aquarium
When you first set up an aquarium there is no biological balance. It takes weeks, if not months to achieve. Maintaining a planted aquariums is more involved than managing anything terrestrial. Do keep this in mind before you venture in this hobby! 

The lesson: Patience is key, start slow, and manage nutrients in small amounts. Any changes made needs to be incremental and allow at least a couple of weeks to evaluate the effects. Be consistent, don’t make sudden drastic changes to the system. Take notes and keep a log of any changes in dosing, daylight hours, water parameters on a weekly basis. Eventually your planted aquarium will find balance and go on auto-pilot.

### Algae!
Ah! the annoying microphyte. you may experience this in some form or the other. The presence of algae alone sn't a problem, it does highlight imbalances that may need to be corrected. I will recommend Dennis Wong's excellent [articles](https://www.2hraquarist.com/pages/algae-free) on managing algae. 

## Further Resources

* [2HrAuarist](https://www.2hraquarist.com/): Dennis wong's content provided much of the inspiration for helping me make choices. I discovered his content much later to take full advantage of his valuable knowledge. I highly recommend going through his youttube channel
* [Aquarium Co-Op](https://www.youtube.com/user/AquariumCoop): I didn't intend to keep much 

Strong light produces rapid plant growth, which in turn, puts an increased demand on fertilizers and CO2. Adding too much fertilizer can cause algae blooms, and too much CO2 can cause fluctuations in pH. 
