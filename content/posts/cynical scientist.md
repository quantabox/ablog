---
author: "akbar m"
title: "Cynical science"
date: "2020-07-02"
description: "A cynical view of data science and current affairs in AI technology."
draft: true
tags: [
    "data science",
    "cynicism",
    "objective",
    "hype"
]
---
