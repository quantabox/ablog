---
title: "Hello, I'm Akbar"
date: "2020-07-19"
---
I'm sharing this space to express a longstanding desire to write. While I intend to focus on technology and business domains, my journals here will explore other fleeting themes that I'm curious about, including Aquatic plants, keyboards, and books! I'll share working machine learning code snippets that I hope you find useful.

I have a background in Mathematical Statistics and Management. I have spent more than a decade tackling machine learning problems for large enterprises. As of 2021, I'm helping tackle complex problems for the US fortune 100 C-suite using a cross-functional approach of AI, engineering, behavioral science! 
